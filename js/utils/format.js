
export function format(n) {
  return n.toFixed(0).replace(/./g, (c, i, a) =>
    (i > 0 && c !== '.' && (a.length - i) % 3 === 0 ? `,${c}` : c)
  );
}

export function formatGia(n = 0, currency) {
  return `${format(n)} ${currency}`;
}

export function formatDollar(n = 0, currency) {
  return `${currency} ${format(n)}`;
}
