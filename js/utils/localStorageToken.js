
const TOKEN_KEY = 'TOKEN';

class Token {
  setToken = (token = null) => {
    if (token && typeof token === 'string') {
      localStorage.setItem(TOKEN_KEY, token);
    } else {
      throw new Error('token don\'t string');
    }
  }

  getToken = () => localStorage.getItem(TOKEN_KEY);

  removeToken = () => {
    localStorage.removeItem(TOKEN_KEY);
  }

  isLoggedIn = () => {
    const token = this.getToken();
    return token !== null && token !== undefined;
  }
}

const token = new Token();

export default token;
