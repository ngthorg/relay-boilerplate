import Relay from 'react-relay';
import React, { Component, PropTypes } from 'react';
import BoxItem from './BoxItem';


class ListBox extends Component {
  static propTypes = {
    relay: PropTypes.shape({
      variables: PropTypes.shape({
        first: PropTypes.number.isRequired,
        skip: PropTypes.number.isRequired,
      }).isRequired,
      setVariables: PropTypes.func.isRequired,
    }).isRequired,
    viewer: PropTypes.shape({
      boxs: PropTypes.shape({
        edges: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired,
        pageInfo: PropTypes.shape({
          endCursor: PropTypes.string,
        }).isRequired,
      }).isRequired,
    }).isRequired,
  };

  Load = () => {
    if (this.props.viewer.boxs.pageInfo.endCursor) {
      this.props.relay.setVariables({
        skip: this.props.relay.variables.skip + this.props.relay.variables.first,
      });
    }
  }

  renderList() {
    return this.props.viewer.boxs.edges.map(item => (
      <BoxItem
        key={item.node.id}
        box={item.node}
      />
    ));
  }

  render() {
    return (
      <div>
        <h3>List boxs:</h3>
        {this.renderList()}
        <button className="btn btn-outline-success" onClick={this.Load}>Load</button>
      </div>
    );
  }
}


export default Relay.createContainer(ListBox, {
  initialVariables: {
    first: 2,
    skip: 0,
  },
  prepareVariables(prevVariables) {
    return {
      ...prevVariables,
    };
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        boxs(first: $first, skip: $skip) {
          edges {
            node {
              id
              ${BoxItem.getFragment('box')}
            }
          }
          pageInfo {
            endCursor
          }
        }
      }
    `,
  },
});
