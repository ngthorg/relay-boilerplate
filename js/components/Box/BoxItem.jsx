import React, { Component, PropTypes } from 'react';
import Relay from 'react-relay';
import { Link, withRouter } from 'react-router';
import UpdateBoxMutation from '../../mutations/UpdateBoxMutation';
import RemoveBoxMutation from '../../mutations/RemoveBoxMutation';


class BoxItem extends Component {
  static propTypes = {
    relay: PropTypes.shape({
      commitUpdate: PropTypes.func.isRequired,
    }).isRequired,
    box: PropTypes.shape({
      title: PropTypes.string,
    }),
    router: PropTypes.shape({
      replace: PropTypes.func.isRequired,
    }).isRequired,
    isReplace: PropTypes.bool,
  };

  static defaultProps = {
    isReplace: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      title: props.box.title,
      update: false,
    };
  }

  onChange = (e) => {
    this.setState({
      title: e.target.value,
    });
  }

  endUpdate = () => {
    this.setState({
      update: false,
    });
  }

  handleUpdateBox = () => {
    this.setState({
      update: true,
    }, () => {
      Relay.Store.commitUpdate(
        new UpdateBoxMutation({ box: this.props.box, title: this.state.title }), {
          onSuccess: this.endUpdate,
          onFailure: this.endUpdate,
        }
      );
    });
  }

  handleRemoveBox = () => {
    const { box } = this.props;
    this.props.relay.commitUpdate(
      new RemoveBoxMutation({ box }), {
        onSuccess: () => {
          if (this.props.isReplace) {
            this.props.router.replace('/boxs');
          }
        },
      }
    );
  }

  render() {
    const { box } = this.props;
    return (
      <div className="form-group row">
        <span className="col-xs-2 col-form-label">
          <Link to={`/boxs/${box.id}`}>{box.title}</Link>
        </span>
        <div className="col-xs-8">
          <input className="form-control" value={this.state.title} onChange={this.onChange} />
        </div>
        <div className="col-xs-2">
          <button
            disabled={this.state.update || this.props.box.title === this.state.title}
            onClick={this.handleUpdateBox}
            className="btn btn-outline-success"
          >
            {this.state.update && (
              <i className="fa fa-spinner fa-pulse" aria-hidden="true" />
            )}
            {' '}
            update
          </button>
        </div>
      </div>
    );
  }
}


export default Relay.createContainer(withRouter(BoxItem), {
  fragments: {
    box: () => Relay.QL`
      fragment on Box {
        id,
        title
        ${UpdateBoxMutation.getFragment('box')}
        ${RemoveBoxMutation.getFragment('box')}
      }
    `,
  },
});
