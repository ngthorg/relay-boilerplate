import React, { Component, PropTypes } from 'react';
import Relay from 'react-relay';
import { connect } from 'react-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { Link, withRouter } from 'react-router';
import UpdateBoxMutation from '../../mutations/UpdateBoxMutation';

const validate = (values) => {
  const errors = {};
  if (!values.title) {
    errors.title = 'Required';
  } else if (values.title.length > 15) {
    errors.title = 'Must be 15 characters or less';
  }
  return errors;
};

class BoxItemForm extends Component {
  static propTypes = {
    box: PropTypes.shape({
      id: PropTypes.string,
      title: PropTypes.string,
    }),
    error: PropTypes.string,
    handleSubmit: PropTypes.func.isRequired,
    pristine: PropTypes.bool.isRequired,
    invalid: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      update: false,
    };
  }

  endUpdate = () => {
    this.setState({
      update: false,
    });
  }

  handleSubmit = data => new Promise((resolve, reject) => {
    this.setState({
      update: true,
    }, () => {
      Relay.Store.commitUpdate(
        new UpdateBoxMutation({ box: this.props.box, title: data.title }), {
          // onSuccess: this.endUpdate,
          // onFailure: this.endUpdate,
          onSuccess: () => {
            reject(new SubmissionError({ title: 'User does not exist', _error: 'Login failed!' }));
            this.endUpdate();
          },
          onFailure: () => {
            reject(new SubmissionError({ title: 'User does not exist', _error: 'Login failed!' }));
            this.endUpdate();
          },
        }
      );
    });
  });

  render() {
    const { handleSubmit, pristine, invalid, submitting } = this.props;
    return (
      <form className="form-group row" onSubmit={handleSubmit(this.handleSubmit)}>
        <span className="col-xs-2 col-form-label">
          <Link to={`/boxs/${this.props.box.id}`}>{this.props.box.title}</Link>
          {' '}
          <span>{this.props.error}</span>
        </span>
        <div className="col-xs-8">
          <Field
            name="title"
            type="text"
            component="input"
            placeholder="Title"
            className="form-control"
          />
        </div>
        <div className="col-xs-2">
          <button
            type="submit"
            disabled={this.state.update || pristine || invalid || submitting}
            className="btn btn-outline-success"
          >
            {this.state.update && (
              <i className="fa fa-spinner fa-pulse" aria-hidden="true" />
            )}
            {' '}
            update
          </button>
        </div>
      </form>
    );
  }
}

const BoxItemFormContainer = connect(
  (state, props) => ({
    enableReinitialize: true,
    initialValues: props.box,
  }),
)(reduxForm({
  form: 'initializeFromBoxItem',
  validate,
})(BoxItemForm));

export default Relay.createContainer(withRouter(BoxItemFormContainer), {
  fragments: {
    box: () => Relay.QL`
      fragment on Box {
        id,
        title
        ${UpdateBoxMutation.getFragment('box')}
      }
    `,
  },
});
