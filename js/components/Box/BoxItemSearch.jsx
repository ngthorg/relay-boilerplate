import React, { PropTypes } from 'react';
import Relay from 'react-relay';
import { Link } from 'react-router';


function BoxItemSearch(props) {
  return (
    <div>
      <Link to={`/boxs/${props.box.id}`}>{props.box.title}</Link>
    </div>
  );
}

BoxItemSearch.propTypes = {
  box: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }),
};


export default Relay.createContainer(BoxItemSearch, {
  fragments: {
    box: () => Relay.QL`
      fragment on Box {
        id,
        title
      }
    `,
  },
});
