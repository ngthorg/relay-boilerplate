import React, { Component, PropTypes } from 'react';
import { Link, withRouter } from 'react-router';
import Relay from 'react-relay';
import UpdateProductMutation from '../../mutations/UpdateProductMutation';
import RemoveProductMutation from '../../mutations/RemoveProductMutation';


class ProductItem extends Component {
  static propTypes = {
    relay: PropTypes.shape({
      commitUpdate: PropTypes.func.isRequired,
    }).isRequired,
    product: PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string.isRequired,
    }).isRequired,
    router: PropTypes.shape({
      replace: PropTypes.func.isRequired,
    }).isRequired,
    isReplace: PropTypes.bool,
  };

  static defaultProps = {
    isReplace: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      update: false,
      name: props.product.name,
    };
  }

  onChangeName = (e) => {
    this.setState({
      name: e.target.value,
    });
  }

  endUpdate = () => {
    this.setState({
      update: false,
    });
  }

  handleUpdateProduct = () => {
    if (this.props.product.name !== this.state.name) {
      this.setState({
        update: true,
      }, () => {
        Relay.Store.commitUpdate(
          new UpdateProductMutation({
            product: this.props.product,
            newData: { name: this.state.name },
          }), {
            onSuccess: this.endUpdate,
            onFailure: this.endUpdate,
          }
        );
      });
    }
  }

  handleRemoveProduct = () => {
    const { product } = this.props;
    this.props.relay.commitUpdate(
      new RemoveProductMutation({ product }), {
        onSuccess: () => {
          if (this.props.isReplace) {
            this.props.router.replace('/products');
          }
        },
      }
    );
  }

  render() {
    return (
      <div className="form-group row">
        <span className="col-xs-2 col-form-label">
          <Link to={`/products/${this.props.product.id}`}>
            {this.props.product.name}
          </Link>
        </span>
        <div className="col-xs-7">
          <input
            className="form-control"
            value={this.state.name}
            onChange={this.onChangeName}
          />
        </div>
        <div className="col-xs-2">
          <button
            disabled={this.state.update || this.props.product.name === this.state.name}
            onClick={this.handleUpdateProduct}
            className="btn btn-outline-success"
          >
            {this.state.update && (
              <i className="fa fa-spinner fa-pulse" aria-hidden="true" />
            )}
            {' '}
            update
          </button>
        </div>
        <div className="col-xs-1">
          <button
            className="btn btn-outline-success"
            onClick={this.handleRemoveProduct}
          >
            x
          </button>
        </div>
      </div>
    );
  }
}


export default Relay.createContainer(withRouter(ProductItem), {
  fragments: {
    product: () => Relay.QL`
      fragment on Product {
        id
        name
        code
        price
        images
        date
        ${UpdateProductMutation.getFragment('product')}
        ${RemoveProductMutation.getFragment('product')}
      }
    `,
  },
});
