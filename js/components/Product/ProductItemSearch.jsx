import React, { PropTypes } from 'react';
import Relay from 'react-relay';
import { Link } from 'react-router';


function ProductItemSearch(props) {
  return (
    <div>
      <Link to={`/products/${props.product.id}`}>{props.product.name}</Link>
    </div>
  );
}

ProductItemSearch.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }),
};


export default Relay.createContainer(ProductItemSearch, {
  fragments: {
    product: () => Relay.QL`
      fragment on Product {
        id,
        name
      }
    `,
  },
});
