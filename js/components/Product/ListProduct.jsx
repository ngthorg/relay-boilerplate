import Relay from 'react-relay';
import React, { Component, PropTypes } from 'react';
import ProductItem from './ProductItem';


class ListProduct extends Component {
  static propTypes = {
    viewer: PropTypes.shape({
      products: PropTypes.shape({
        edges: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired,
      }).isRequired,
    }).isRequired,
  };

  renderList() {
    return this.props.viewer.products.edges.map(item => (
      <ProductItem
        key={item.node.id}
        product={item.node || {}}
      />
    ));
  }

  render() {
    return (
      <div>
        <h3>List Product:</h3>
        {this.renderList()}
      </div>
    );
  }
}


export default Relay.createContainer(ListProduct, {
  initialVariables: {
    boxId: null,
  },
  prepareVariables(prevVariables) {
    return {
      ...prevVariables,
    };
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        products(first: 2147483647, boxId: $boxId) {
          edges {
            node {
              id
              ${ProductItem.getFragment('product')}
            }
          }
        }
      }
    `,
  },
});
