import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Field, reduxForm, SubmissionError, initialize } from 'redux-form';

const validate = (values) => {
  const errors = {};
  if (!values.name) {
    errors.name = 'Required';
  } else if (values.name.length > 15) {
    errors.name = 'Must be 15 characters or less';
  }
  if (!values.code) {
    errors.code = 'Required';
  } else if (values.code.length > 7) {
    errors.code = 'Must be 15 characters or less';
  }
  if (!values.price) {
    errors.price = 'Required';
  }
  return errors;
};

@connect((state, props) => ({
  enableReinitialize: false,
  keepDirtyOnReinitialize: true,
  initialValues: props.product || {},
}))
@reduxForm({
  form: 'product/item',
  validate,
})
@withRouter

export default class FormCreateProduct extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    reset: PropTypes.func.isRequired,
    pristine: PropTypes.bool.isRequired,
    invalid: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
    isProduct: PropTypes.bool,
    onRemove: PropTypes.func,

    dispatch: PropTypes.func.isRequired,
    product: PropTypes.shape({}).isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      isSubmit: false,
    };
  }

  componentDidMount() {
    this.props.dispatch(initialize('product/item', this.props.product, true));
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.product !== nextProps.product) {
      this.props.dispatch(initialize('product/item', this.props.product, true));
    }
  }

  handleSubmit = (data) => {
    this.setState({
      isSubmit: true,
    });

    return this.props.onSubmit(data)
      .then(() => {
        this.props.reset();
        this.setState({
          isSubmit: false,
        });
      })
      .catch((err) => {
        console.log('catch', err);
        this.setState({
          isSubmit: false,
        });
        throw new SubmissionError({ name: 'User does not exist', _error: 'Login failed!' });
      });
  }

  render() {
    const { handleSubmit, pristine, invalid, submitting, isProduct } = this.props;
    return (
      <form className="form-group row" onSubmit={handleSubmit(this.handleSubmit)}>
        <div className="col-xs-3">
          <Field
            name="name"
            type="text"
            component="input"
            placeholder="Name"
            className="form-control"
          />
        </div>
        <div className="col-xs-3">
          <Field
            name="code"
            type="text"
            component="input"
            placeholder="Code"
            className="form-control"
          />
        </div>
        <div className="col-xs-3">
          <Field
            name="price"
            type="number"
            min="2000"
            component="input"
            placeholder="Price"
            className="form-control"
          />
        </div>
        {!this.props.onRemove && !isProduct && (
          <div className="col-xs-3">
            <button
              type="submit"
              className="btn btn-success"
              disabled={this.state.isSubmit || pristine || invalid || submitting}
            >
              {this.state.isSubmit && (
                <i className="fa fa-spinner fa-pulse" aria-hidden="true" />
              )}
              {' '}
              submit
            </button>
          </div>
        )}
        {this.props.onRemove && isProduct && (
          <div className="col-xs-2">
            <button
              type="submit"
              className="btn btn-success"
              disabled={this.state.isSubmit || pristine || invalid || submitting}
            >
              {this.state.isSubmit && (
                <i className="fa fa-spinner fa-pulse" aria-hidden="true" />
              )}
              {' '}
              update
            </button>
          </div>
        )}
        {this.props.onRemove && isProduct && (
          <div className="col-xs-1">
            <button
              className="btn btn-outline-success"
              onClick={(e) => {
                e.preventDefault();
                this.props.onRemove();
              }}
            >
              x
            </button>
          </div>
        )}
      </form>
    );
  }
}
