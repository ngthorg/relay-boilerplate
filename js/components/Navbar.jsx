import React from 'react';
import { Link, IndexLink } from 'react-router';

export default function Navbar() {
  return (
    <nav className="navbar navbar-full navbar-light bg-faded">
      <div className="nav navbar-nav">
        <IndexLink className="nav-item nav-link" activeClassName="active" to="/">Home</IndexLink>
        <Link className="nav-item nav-link" activeClassName="active" to="/boxs">Boxs</Link>
        <Link className="nav-item nav-link" activeClassName="active" to="/products">Products</Link>
      </div>
    </nav>
  );
}
