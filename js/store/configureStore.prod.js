import { createStore, applyMiddleware, compose } from 'redux';
import promiseMiddleware from '../utils/promiseMiddleware';
import Reducers from '../reducers';


export default function configureStore(initialState = {}) {
  const finalCreateStore = compose(
    applyMiddleware(promiseMiddleware)
  )(createStore);

  const store = finalCreateStore(Reducers, initialState);

  return store;
}
