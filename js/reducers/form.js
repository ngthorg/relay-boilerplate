/* @flow */
import * as ActionTypes from '../constants/ActionTypes';

type State = Object;

type Action = {
  type: string,
  data: Object,
};

export const initialFormProduct = (state: State = {}, action: Action) => {
  switch (action.type) {
    case ActionTypes.INITIAL_PRODUCT:
      return {
        data: action.data,
      };
    default:
      return state;
  }
};

export const initialFormBox = (state: State = {}, action: Action) => {
  switch (action.type) {
    case ActionTypes.INITIAL_BOX:
      return {
        data: action.data,
      };
    default:
      return state;
  }
};
