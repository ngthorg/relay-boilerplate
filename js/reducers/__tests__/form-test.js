import * as form from '../form';
import * as ActionTypes from '../../constants/ActionTypes';

// jest.unmock('../form');
// jest.unmock('../../constants/ActionTypes');

describe('reducer: form', () => {
  it('initialFormAccount', () => {
    const data = { id: '123', name: 'ngthorg' };
    expect(
      form.initialFormAccount({}, {
        type: ActionTypes.INITIAL_ACCOUNT,
        data,
      })
    ).toEqual({
      data,
    });
  });

  it('initialFormProfile', () => {
    const data = { id: '123', name: 'ngthorg' };
    expect(
      form.initialFormProfile({}, {
        type: ActionTypes.INITIAL_PROFILE,
        data,
      })
    ).toEqual({
      data,
    });
  });
});
