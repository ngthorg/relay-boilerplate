/* @flow */
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import * as form from './form';

const rootReducer = combineReducers({
  form: formReducer,
  ...form,
});

export default rootReducer;
