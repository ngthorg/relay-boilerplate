import React, { Component, PropTypes } from 'react';
import Relay from 'react-relay';
import merge from 'lodash.merge';
import DocumentMeta from 'react-document-meta';
import ProductItem from '../components/Product/ProductItem';
import BoxItemForm from '../components/Box/BoxItemForm';


class ProductId extends Component {
  static propTypes = {
    node: PropTypes.shape({}),
  };

  static defaultProps = {
    node: {
      name: 'Product',
      box: {},
    },
  };

  render() {
    const defaultValue = { name: 'Product', box: {} };
    const node = merge(defaultValue, this.props.node);

    return (
      <div className="container" style={{ marginTop: 20 }}>
        <DocumentMeta title={node.name} />

        <ProductItem
          isReplace
          product={node}
        />

        <h3>Box</h3>
        <BoxItemForm box={node.box} />
      </div>
    );
  }
}


export default Relay.createContainer(ProductId, {
  fragments: {
    node: () => Relay.QL`
      fragment on Product {
        name
        ${ProductItem.getFragment('product')}
        box {
          ${BoxItemForm.getFragment('box')}
        }
      }
    `,
  },
});
