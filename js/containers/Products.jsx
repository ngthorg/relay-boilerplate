import React, { Component, PropTypes } from 'react';
import Relay from 'react-relay';
import shallowCompare from 'react/lib/shallowCompare';
import DocumentMeta from 'react-document-meta';
import ListProduct from '../components/Product/ListProduct';


class Products extends Component {
  static propTypes = {
    viewer: PropTypes.shape({}).isRequired,
  };

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    return (
      <div className="container" style={{ marginTop: 20 }}>
        <DocumentMeta title="Products" />

        <ListProduct
          viewer={this.props.viewer}
        />
      </div>
    );
  }
}


export default Relay.createContainer(Products, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        ${ListProduct.getFragment('viewer')}
      }
    `,
  },
});
