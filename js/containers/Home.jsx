/* eslint no-underscore-dangle:0 */
import React, { Component, PropTypes } from 'react';
import Relay from 'react-relay';
import get from 'lodash.get';
import shallowCompare from 'react/lib/shallowCompare';
import DocumentMeta from 'react-document-meta';
import BoxItemSearch from '../components/Box/BoxItemSearch';
import ProductItemSearch from '../components/Product/ProductItemSearch';

class Home extends Component {
  static propTypes = {
    relay: PropTypes.shape({
      setVariables: PropTypes.func.isRequired,
    }).isRequired,
    viewer: PropTypes.shape({}).isRequired,
  };

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  onChange = (e) => {
    this.props.relay.setVariables({
      searchText: e.target.value,
    });
  }

  renderListSearch() {
    return get(this.props.viewer, ['searchs', 'edges'], []).map((item) => {
      switch (item.node.__typename) {
        case 'Box':
          return (
            <BoxItemSearch
              key={item.node.id}
              box={item.node}
            />
          );
        case 'Product':
          return (
            <ProductItemSearch
              key={item.node.id}
              product={item.node}
            />
          );
        default:
          return (
            <div key={item.node.id}>
              <p>{item.node.__typename}</p>
            </div>
          );
      }
    });
  }

  render() {
    return (
      <div className="container" style={{ marginTop: 20 }}>
        <DocumentMeta title="Home" />

        <h1>Home</h1>

        <input type="text" onChange={this.onChange} placeholder="Search" />

        {this.renderListSearch()}

      </div>
    );
  }
}


export default Relay.createContainer(Home, {
  initialVariables: {
    searchText: '',
    showSearch: false,
  },
  prepareVariables({ searchText }) {
    const showSearch = (searchText !== '');
    return {
      searchText,
      showSearch,
    };
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        searchs(first: 2147483647, text: $searchText) @include(if: $showSearch) {
          edges {
            node {
              __typename
              ...on Box {
                id
                ${BoxItemSearch.getFragment('box')}
              }
              ...on Product {
                id
                ${ProductItemSearch.getFragment('product')}
              }
            }
          }
        }
      }
    `,
  },
});
