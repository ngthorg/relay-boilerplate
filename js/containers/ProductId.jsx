import React, { Component, PropTypes } from 'react';
import Relay from 'react-relay';
import { withRouter } from 'react-router';
import merge from 'lodash.merge';
import DocumentMeta from 'react-document-meta';
import FormCreateProduct from '../components/Product/FormCreateProduct-test';
import BoxItemForm from '../components/Box/BoxItemForm';
import UpdateProductMutation from '../mutations/UpdateProductMutation';
import RemoveProductMutation from '../mutations/RemoveProductMutation';


class ProductId extends Component {
  static propTypes = {
    node: PropTypes.shape({}),
    relay: PropTypes.shape({
      commitUpdate: PropTypes.func.isRequired,
    }).isRequired,
    router: PropTypes.shape({
      replace: PropTypes.func.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    node: {
      name: 'Product',
      box: {},
    },
  };

  onSubmit = data => new Promise((resolve, reject) => {
    this.props.relay.commitUpdate(
      new UpdateProductMutation({ product: this.props.node, newData: data }), {
        onSuccess: resolve,
        onFailure: reject,
      }
    );
  });

  onRemove = () => {
    const { node } = this.props;
    this.props.relay.commitUpdate(
      new RemoveProductMutation({ product: node }), {
        onSuccess: () => {
          this.props.router.replace('/products');
        },
      }
    );
  }

  render() {
    const defaultValue = { name: 'Product', box: {} };
    const node = merge(defaultValue, this.props.node);

    return (
      <div className="container" style={{ marginTop: 20 }}>
        <DocumentMeta title={node.name} />

        <FormCreateProduct
          isReplace
          product={node}
          onSubmit={this.onSubmit}
          isProduct
          onRemove={this.onRemove}
        />

        <h3>Box</h3>
        <BoxItemForm box={node.box} />
      </div>
    );
  }
}


export default Relay.createContainer(withRouter(ProductId), {
  fragments: {
    node: () => Relay.QL`
      fragment on Product {
        name
        id
        name
        code
        price
        images
        date
        box {
          ${BoxItemForm.getFragment('box')}
        }
        ${UpdateProductMutation.getFragment('product')}
        ${RemoveProductMutation.getFragment('product')}
      }
    `,
  },
});
