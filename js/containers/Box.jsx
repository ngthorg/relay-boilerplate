import keycode from 'keycode';
import React, { Component, PropTypes } from 'react';
import Relay from 'react-relay';
import DocumentMeta from 'react-document-meta';
import ListBox from '../components/Box/ListBox';
import AddBoxMutation from '../mutations/AddBoxMutation';

class Home extends Component {
  static propTypes = {
    viewer: PropTypes.shape({}).isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      text: '',
    };
  }

  onChange = (e) => {
    this.setState({ text: e.target.value });
  }

  handleKeyDown = (e) => {
    if (e.keyCode === keycode.codes.enter) {
      const title = e.target.value;
      const { viewer } = this.props;
      Relay.Store.commitUpdate(new AddBoxMutation({ viewer, newData: { title } }));
    }
  }

  render() {
    return (
      <div className="container" style={{ marginTop: 20 }}>
        <DocumentMeta title="Home" />

        <div className="form-group">
          <input
            type="text"
            className="form-control"
            value={this.state.text}
            onKeyDown={this.handleKeyDown}
            onChange={this.onChange}
            placeholder="Create box"
          />
        </div>

        <ListBox viewer={this.props.viewer} />
      </div>
    );
  }
}


export default Relay.createContainer(Home, {
  initialVariables: {
    first: 2,
    skip: 0,
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        ${ListBox.getFragment('viewer')}
        ${AddBoxMutation.getFragment('viewer')}
      }
    `,
  },
});
