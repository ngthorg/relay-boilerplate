import React, { Component, PropTypes } from 'react';
import Relay from 'react-relay';
import merge from 'lodash.merge';
import DocumentMeta from 'react-document-meta';
import BoxItem from '../components/Box/BoxItem';
import ListProduct from '../components/Product/ListProduct';
import FromCreateProduct from '../components/Product/FormCreateProduct';
import AddProductMutation from '../mutations/AddProductMutation';


class BoxId extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    node: PropTypes.shape({
    }),
    relay: PropTypes.shape({
      commitUpdate: PropTypes.func.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    node: {
      title: 'Box',
      viewer: {},
    },
  };

  createProduct = (data) => {
    const { node: { id: boxId, viewer } } = this.props;
    return new Promise((resolve, reject) => {
      this.props.relay.commitUpdate(
        new AddProductMutation({ newData: { ...data, boxId }, viewer }), {
          onSuccess: resolve,
          onFailure: reject,
        }
      );
    });
  }

  render() {
    const defaultValue = { title: 'Box', viewer: {} };
    const node = merge(defaultValue, this.props.node);

    return (
      <div className="container" style={{ marginTop: 20 }}>
        <DocumentMeta title={node.title} />

        <BoxItem
          isReplace
          box={node}
        />

        <FromCreateProduct
          onSubmit={this.createProduct}
        />

        <ListProduct
          boxId={this.props.id}
          viewer={node.viewer}
        />
      </div>
    );
  }
}


export default Relay.createContainer(BoxId, {
  initialVariables: {
    id: null,
  },
  fragments: {
    node: ({ id }) => Relay.QL`
      fragment on Box {
        id
        title
        ${BoxItem.getFragment('box')}
        viewer {
          ${AddProductMutation.getFragment('viewer')}
          ${ListProduct.getFragment('viewer', { boxId: id })}
        }
      }
    `,
  },
});
