import React from 'react';
import { Link } from 'react-router';
import DocumentMeta from 'react-document-meta';

const meta = {
  title: '404 Not Found!',
};

const NotFound = () => (
  <div className="container" style={{ marginTop: 20 }}>
    <DocumentMeta {...meta} />
    <h4 className="text-center">404 Not Found!</h4>
    <div className="text-center">
      <Link to="/">Go Home!</Link>
    </div>
  </div>
);

export default NotFound;
