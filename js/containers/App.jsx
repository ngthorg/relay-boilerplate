import React, { PropTypes } from 'react';
import Navbar from '../components/Navbar';


export default function App(props) {
  return (
    <div>
      <Navbar />
      {props.children}
    </div>
  );
}

App.propTypes = {
  children: PropTypes.node.isRequired,
};
