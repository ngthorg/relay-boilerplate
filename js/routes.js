import React from 'react';
import Relay from 'react-relay';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, applyRouterMiddleware, browserHistory } from 'react-router';
import useRelay from 'react-router-relay';

import App from './containers/App';
import Home from './containers/Home';
import Box from './containers/Box';
import BoxId from './containers/BoxId';
import Products from './containers/Products';
import ProductId from './containers/ProductId';
import NotFound from './containers/NotFound';

import ViewerQueries from './queries/ViewerQueries';
import NodeQueries from './queries/NodeQueries';

type Props = {
  store: Object,
};

const Root = ({ store }: Props) => (
  <Provider store={store}>
    <Router
      history={browserHistory}
      render={applyRouterMiddleware(useRelay)}
      environment={Relay.Store}
    >
      <Route
        path="/" component={App}
      >
        <IndexRoute component={Home} queries={ViewerQueries} />
        <Route path="boxs">
          <IndexRoute component={Box} queries={ViewerQueries} />
          <Route path=":id" component={BoxId} queries={NodeQueries} />
        </Route>
        <Route path="products">
          <IndexRoute component={Products} queries={ViewerQueries} />
          <Route path=":id" component={ProductId} queries={NodeQueries} />
        </Route>
        <Route path="*" component={NotFound} />
      </Route>
    </Router>
  </Provider>
);

export default Root;
