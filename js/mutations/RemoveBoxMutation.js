import Relay from 'react-relay';


export default class RemoveBoxMutation extends Relay.Mutation {
  static fragments = {
    box: () => Relay.QL`
      fragment on Box {
        id
        viewer {
          id
        }
      }
    `,
  };

  getMutation = () => Relay.QL`mutation {removeBox}`;

  getFatQuery = () => Relay.QL`
    fragment on BoxRemovePayload @relay(pattern: true) {
      deletedBoxId,
      viewer {
        boxs
      }
    }
  `;

  getConfigs() {
    return [{
      type: 'NODE_DELETE',
      parentName: 'viewer',
      parentID: this.props.box.viewer.id,
      connectionName: 'boxs',
      deletedIDFieldName: 'deletedBoxId',
    }];
  }

  getVariables() {
    return {
      id: this.props.box.id,
    };
  }

  getOptimisticResponse() {
    return {
      deletedBoxId: this.props.box.id,
    };
  }
}
