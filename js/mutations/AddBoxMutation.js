import Relay from 'react-relay';

export default class AddBoxMutation extends Relay.Mutation {
  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
      }
    `,
  };

  getMutation = () => Relay.QL`mutation {createBox}`;

  getFatQuery = () => Relay.QL`
    fragment on BoxCreatePayload @relay(pattern: true) {
      boxEdge,
      viewer {
        boxs
      }
    }
  `;

  getConfigs() {
    return [{
      type: 'RANGE_ADD',
      parentName: 'viewer',
      parentID: this.props.viewer.id,
      connectionName: 'boxs',
      edgeName: 'boxEdge',
      rangeBehaviors: () => 'prepend',
    }];
  }

  getVariables() {
    return {
      title: this.props.newData.title,
    };
  }

  getOptimisticResponse() {
    const { viewer, newData } = this.props;
    return {
      boxEdge: {
        node: {
          title: newData.title,
        },
      },
      viewer: {
        id: viewer.id,
      },
    };
  }
}
