import Relay from 'react-relay';

export default class RemoveProductMutation extends Relay.Mutation {
  static fragments = {
    product: () => Relay.QL`
      fragment on Product {
        id
        viewer {
          id
        }
      }
    `,
  };

  getMutation = () => Relay.QL`mutation {removeProduct}`;

  getFatQuery = () => Relay.QL`
    fragment on ProductRemovePayload @relay(pattern: true) {
      deletedProductId
      viewer {
        products
      }
    }
  `;

  getConfigs() {
    return [{
      type: 'NODE_DELETE',
      parentName: 'viewer',
      parentID: this.props.product.viewer.id,
      connectionName: 'products',
      deletedIDFieldName: 'deletedProductId',
    }];
  }

  getVariables() {
    return {
      id: this.props.product.id,
    };
  }

  getOptimisticResponse() {
    return {
      deletedProductId: this.props.product.id,
    };
  }
}
