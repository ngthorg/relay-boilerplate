import Relay from 'react-relay';

export default class AddTodoMutation extends Relay.Mutation {
  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
      }
    `,
  };

  getMutation = () => Relay.QL`mutation {createProduct}`;

  getFatQuery = () => Relay.QL`
    fragment on ProductCreatePayload @relay(pattern: true) {
      productEdge,
      viewer {
        products
      },
    }
  `;

  getConfigs() {
    return [{
      type: 'RANGE_ADD',
      parentName: 'viewer',
      parentID: this.props.viewer.id,
      connectionName: 'products',
      edgeName: 'productEdge',
      rangeBehaviors: () => 'append',
    }];
  }

  getVariables() {
    const { newData } = this.props;
    return {
      name: newData.name,
      code: newData.code,
      price: newData.price,
      boxId: newData.boxId,
    };
  }

  getOptimisticResponse() {
    const { viewer, newData } = this.props;
    return {
      productEdge: {
        node: {
          name: newData.name,
          code: newData.code,
          price: newData.price,
        },
      },
      viewer: {
        id: viewer.id,
      },
    };
  }
}
