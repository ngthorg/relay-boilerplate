import Relay from 'react-relay';
import _ from 'lodash';

export default class UpdateProductMutation extends Relay.Mutation {
  static fragments = {
    product: () => Relay.QL`
      fragment on Product {
        id
      }
    `,
  };

  getMutation = () => Relay.QL`mutation {updateProduct}`;

  getFatQuery = () => Relay.QL`
    fragment on ProductUpdatePayload @relay(pattern: true) {
      product {
        name
      }
    }
  `;

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        product: this.props.product.id,
      },
    }];
  }

  getVariables() {
    return {
      id: this.props.product.id,
      ..._.pick(this.props.newData, ['name', 'code', 'price']),
    };
  }

  getOptimisticResponse() {
    return {
      product: {
        id: this.props.product.id,
        ..._.pick(this.props.newData, ['name', 'code', 'price']),
      },
    };
  }
}
