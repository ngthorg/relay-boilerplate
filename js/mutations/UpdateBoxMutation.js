import Relay from 'react-relay';

export default class UpdateBoxMutation extends Relay.Mutation {
  static fragments = {
    box: () => Relay.QL`
      fragment on Box {
        id
      }
    `,
  };

  getMutation = () => Relay.QL`mutation {updateBox}`;

  getFatQuery = () => Relay.QL`
    fragment on BoxUpdatePayload @relay(pattern: true) {
      box {
        title
      }
    }
  `;

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        box: this.props.box.id,
      },
    }];
  }

  getVariables() {
    return {
      id: this.props.box.id,
      title: this.props.title,
    };
  }

  getOptimisticResponse() {
    return {
      box: {
        id: this.props.box.id,
        title: this.props.title,
      },
    };
  }
}
