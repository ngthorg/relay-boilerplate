import * as ActionTypes from '../constants/ActionTypes';

export const loadBox = data => ({ type: ActionTypes.INITIAL_BOX, data });
export const loadProduct = data => ({ type: ActionTypes.INITIAL_PRODUCT, data });
