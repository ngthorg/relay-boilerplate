import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
// import Relay from 'react-relay';
// import { Router, applyRouterMiddleware, browserHistory } from 'react-router';
// import useRelay from 'react-router-relay';

import configureStore from '../js/store/configureStore';
import Root from '../js/routes';

const store: Object = configureStore({});

ReactDOM.render(
  <Root
    store={store}
  />,
  document.getElementById('root')
);

// ReactDOM.render(
//   <Router
//     history={browserHistory}
//     routes={routes}
//     render={applyRouterMiddleware(useRelay)}
//     // forceFetch
//     environment={Relay.Store}
//   />,
//   document.getElementById('root')
// );
