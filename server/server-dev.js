import path from 'path';
import express from 'express';
import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import webpackConfig from '../webpack.config-dev';

const APP_PORT = process.env.APP_PORT || 3000;
const GRAPHQL_PORT = process.env.GRAPHQL_PORT || 8000;

// Serve the Relay app
const app = new WebpackDevServer(webpack(webpackConfig), {
  contentBase: '/public/',
  proxy: {
    '/graphql': `http://localhost:${GRAPHQL_PORT}`,
  },
  publicPath: '/',
  stats: { colors: true },
  hot: true,
  historyApiFallback: true,
});

// Serve static resources
app.use('/', express.static(path.resolve(__dirname, '../public')));

app.listen(APP_PORT, () => {
  console.info(`Relay is listening on port ${APP_PORT}`);
});
