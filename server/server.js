import path from 'path';
import express from 'express';
import graphHTTP from 'express-graphql';
import historyApiFallback from 'connect-history-api-fallback';
import schema from './data/schema';

const APP_PORT = process.env.APP_PORT || 3000;

const app = express();

app.use(historyApiFallback());
app.use('/', express.static(path.resolve(__dirname, '../public')));

// Serve static resources
app.use('/graphql', graphHTTP({ schema }));

app.listen(APP_PORT, () => {
  console.info(`Relay is listening on port ${APP_PORT}`);
});
