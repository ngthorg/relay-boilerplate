import config from '../../config/config.thinky';
const Thinky = require('thinky')(config);
const { r, type } = Thinky;


const Box = Thinky.createModel('Box', {
  id: type.string(),
  title: type.string().required(),
  type: type.string().default('box'),
});

const Product = Thinky.createModel('Product', {
  id: type.string(),
  name: type.string().required(),
  code: type.string().required(),
  price: type.number().required(),
  images: type.array().schema(type.string()),
  date: type.date().default(r.now()),
  boxId: type.string().required(),
  type: type.string().default('product'),
});

// Join the models
Box.hasMany(Product, 'product', 'id', 'boxId');
Product.belongsTo(Box, 'box', 'boxId', 'id');

// Make sure that an index on date is available
Product.ensureIndex('date');

module.exports = Thinky;
