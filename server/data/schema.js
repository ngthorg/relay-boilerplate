import {
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLSchema,
  GraphQLString,
  GraphQLNonNull,
  GraphQLObjectType,
} from 'graphql';
import {
  offsetToCursor,
  fromGlobalId,
  globalIdField,
  nodeDefinitions,
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  connectionFromPromisedArray,
  cursorForObjectInConnection,
  mutationWithClientMutationId,
} from 'graphql-relay';
import Loader from './loader';
import Db from './db';
const loader = new Loader();


const { nodeField, nodeInterface } = nodeDefinitions(globalId => {
  const { id, type } = fromGlobalId(globalId);

  switch (type) {
    case 'Box':
      return loader.box.load(id);
    case 'Product':
      return loader.product.load(id);
    default:
      return null;
  }
}, obj => {
  switch (obj.type) {
    case 'box':
      return boxType;
    case 'product':
      return productType;
    default:
      return null;
  }
});

function getCursor(dataList, item) {
  for (const i of dataList) {
    if (i.id === item.id) {
      return cursorForObjectInConnection(dataList, i);
    }
  }
  return null;
}

const boxType = new GraphQLObjectType({
  name: 'Box',
  description: 'This represents a Box',
  fields: () => ({
    id: globalIdField('Box'),
    title: { type: GraphQLString, resolve: box => box.title },
    products: {
      type: ProductsConnection,
      args: {
        ...connectionArgs,
      },
      resolve(box, args) {
        return connectionFromArray(box.product, args);
      },
    },
  }),
  interfaces: [nodeInterface],
});

const productType = new GraphQLObjectType({
  name: 'Product',
  description: 'This represents a Product',
  fields: () => ({
    id: globalIdField('Product'),
    name: { type: GraphQLString, resolve: product => product.name },
    code: { type: GraphQLString, resolve: product => product.code },
    price: { type: GraphQLInt, resolve: product => product.price },
    boxId: { type: GraphQLString, resolve: product => product.boxId },
    images: { type: new GraphQLList(GraphQLString), resolve: product => product.images },
    date: { type: GraphQLString, resolve: product => product.date },
    box: { type: boxType, resolve: product => product.box },
  }),
  interfaces: [nodeInterface],
});

// Edge
const { connectionType: BoxsConnection, edgeType: BoxEdge } = connectionDefinitions({
  name: 'Box', nodeType: boxType,
});
const { connectionType: ProductsConnection } = connectionDefinitions({
  name: 'Product', nodeType: productType,
});

const Query = new GraphQLObjectType({
  name: 'Query',
  description: 'This is a root query',
  fields: () => ({
    node: nodeField,
    boxs: {
      type: BoxsConnection,
      args: {
        ...connectionArgs,
      },
      resolve(_, args) {
        console.log('args', args);
        return connectionFromPromisedArray(loader.boxs.load('allBox'), args);
      },
    },
    products: {
      type: ProductsConnection,
      args: {
        ...connectionArgs,
      },
      resolve(_, args) {
        return connectionFromPromisedArray(loader.products.load('allProduct'), args);
      },
    },
  }),
});


const AddBoxMutation = mutationWithClientMutationId({
  name: 'AddBox',
  inputFields: {
    title: { type: new GraphQLNonNull(GraphQLString) },
  },
  outputFields: {
    boxEdge: {
      type: BoxEdge,
      resolve(box) {
        return loader.boxs.load('allBox').then(boxs => {
          console.log('cursor', getCursor(boxs, box));
          console.log('cursor2', cursorForObjectInConnection(boxs, box));
          console.log('offsetToCursor', offsetToCursor(1));

          return {
            cursor: getCursor(boxs, box),
            node: box,
          };
        });
      },
    },
    boxs: {
      type: BoxsConnection,
      args: {
        ...connectionArgs,
      },
      resolve(_, args) {
        console.log('args', args);
        return connectionFromPromisedArray(loader.boxs.load('allBox'), args);
      },
    },
  },
  mutateAndGetPayload({ title }) {
    loader.boxs.clear('allBox');

    console.log('mutateAndGetPayload', title);
    return Db.models.Box({ title, product: [] }).save();
  },
});

const RemoveBoxMutation = mutationWithClientMutationId({
  name: 'RemoveBox',
  inputFields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
  },
  outputFields: {
    deletedBoxId: {
      type: GraphQLID,
      resolve: ({ id }) => {
        console.log('deletedBoxId', id);
        return id;
      },
    },
    boxs: {
      type: BoxsConnection,
      args: {
        ...connectionArgs,
      },
      resolve(_, args) {
        console.log('args', args);
        return connectionFromPromisedArray(loader.boxs.load('allBox'), args);
      },
    },
  },
  mutateAndGetPayload: ({ id }) => {
    loader.boxs.clear('allBox');

    const localBoxId = fromGlobalId(id).id;
    return loader.removeBox.load(localBoxId);
    // const localBoxId = fromGlobalId(id).id
    // loader.removeBox.load(localBoxId)
    // return { id }
  },
});

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addBox: AddBoxMutation,
    removeBox: RemoveBoxMutation,
  },
});

const Schema = new GraphQLSchema({
  query: Query,
  mutation: Mutation,
});

export default Schema;
