import DataLoader from 'dataloader';
import Db from './db';
import { toGlobalId } from 'graphql-relay';

// Db.models.Box.get('ao-len-nhat')
//   .changes()
//   .then(function(result) {
//     console.log('getFeed', result)
// 		result.on('change', function(newDoc) {
// 			console.log('newDoc:', newDoc)
// 		})
//   })
//   .error(function (err) {
//     console.error('ERROR!', err);
//   });

function Box() {
  return new DataLoader(ids => {
    return Promise.all(ids.map(id => {
      return Db.models.Box.get(id)
        .getJoin({ product: true })
        .run()
        .then(res => res)
        .catch(err => err);
    }));
  });
}

function RemoveBox() {
  return new DataLoader(ids => {
    return Promise.all(ids.map(id => {
      return Db.models.Box.get(id)
        .getJoin({ product: true })
        .run()
        .then(res => {
          // throw new Error('throw tets Error hihi!')
          return res.delete().then(item => {
            return Object.assign({}, item, { id: toGlobalId('Box', item.id) });
          });
        })
        .catch(err => err);
    }));
  });
}

function Boxs() {
  return new DataLoader(ids => {
    return Promise.all(ids.map(() => {
      return Db.models.Box
				.orderBy('title')
				.getJoin({ product: true })
				.orderBy('title')
				.run()
				.then(res => res)
				.catch(err => err);
    }));
  });
}

function Product() {
  return new DataLoader(ids => {
    return Promise.all(ids.map(id => {
      return Db.models.Product.get(id)
        .getJoin({ box: true })
        .run()
        .then(res => res)
        .catch(err => err);
    }));
  });
}

function Products() {
  return new DataLoader(ids => {
    return Promise.all(ids.map(() => {
      return Db.models.Product
        .getJoin({ box: true })
        .orderBy('title')
        .run()
        .then(res => res)
        .catch(err => err);
    }));
  });
}

module.exports = () => {
  return {
    box: new Box(),
    removeBox: new RemoveBox(),
    boxs: new Boxs(),
    product: new Product(),
    products: new Products(),
  };
};
