import express from 'express';
import Koa from 'koa';
import mount from 'koa-mount';
import convert from 'koa-convert';
import graphqlHTTP from 'koa-graphql';
import http from 'http';
import path from 'path';
import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import webpackConfig from '../webpack.config-dev';
import schema from './data/schema';

const APP_PORT = process.env.APP_PORT || 3000;
const GRAPHQL_PORT = process.env.GRAPHQL_PORT || 8000;

// Expose a GraphQL endpoint
const graphQLServer = new Koa();

graphQLServer.use(mount('/', convert(graphqlHTTP({ schema, pretty: true, graphiql: true }))));

const server = http.createServer(graphQLServer.callback());
server.listen(GRAPHQL_PORT, () => {
  console.log(`GraphQL Server running at http://localhost:${GRAPHQL_PORT}`);
});

// Serve the Relay app
const compiler = webpack(webpackConfig);

const app = new WebpackDevServer(compiler, {
  contentBase: '/public/',
  proxy: {
    '/graphql': `http://localhost:${GRAPHQL_PORT}`,
  },
  publicPath: '/',
  stats: { colors: true },
  hot: true,
  historyApiFallback: true,
});

// Serve static resources
app.use('/', express.static(path.resolve(__dirname, '../public')));

app.listen(APP_PORT, () => {
  console.log(`App is now running on http://localhost:${APP_PORT}`);
});
