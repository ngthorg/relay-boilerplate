
module.exports = {
  development: {
    host: 'localhost',
    port: 28015,
    db: 'shopApp',
  },
  production: {
    host: 'localhost',
    port: 28015,
    db: 'shopApp',
  },
}[process.env.NODE_ENV || 'development'];
