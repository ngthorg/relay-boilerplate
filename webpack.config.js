const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');

const fromImages = path.resolve(__dirname, 'client/images');
const toImages = path.resolve(__dirname, 'public/images');
const styleLoaders = ['css', 'sass', 'postcss'];

module.exports = {
  entry: [
    path.join(__dirname, 'client/index.js'),
    path.join(__dirname, 'client/index.html'),
  ],
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'js/bundle.js',
    publicPath: '/',
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.sass', '.scss', '.css'],
    modulesDirectories: ['client', 'node_modules'],
  },
  module: {
    loaders: [
      {
        test: /\.html$/,
        loader: 'file?name=[name].[ext]',
      },
      {
        test: /\.(js|jsx)$/,
        loader: 'babel',
        exclude: /node_modules/,
      }, {
        test: /\.(css|scss|sass)$/,
        loader: ExtractTextPlugin.extract('style', styleLoaders.join('!'), {
          publicPath: '/',
        }),
      }, {
        test: /\.(png|jpg|jpeg|gif)$/,
        loader: 'url-loader?limit=10240&name=icons/[name].[ext]',
      }, {
        test: /\.(eot|woff2|woff|ttf|svg)$/,
        loader: 'file-loader?name=fonts/[name].[ext]',
      },
    ],
  },
  sassLoader: {
    includePaths: ['client', 'node_modules'],
  },
  postcss: [
    autoprefixer({ browsers: ['last 2 versions'] }),
  ],
  plugins: [
    // Simply copies the files over
    new CopyWebpackPlugin([
      { from: fromImages, to: toImages },
    ]),
    new ExtractTextPlugin('css/style.css'),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      compress: { warnings: false },
      output: { comments: false },
    }),
  ],
};
